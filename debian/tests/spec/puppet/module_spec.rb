require 'spec_helper'
require 'tmpdir'

describe "test cron core module" do
  describe command("puppet apply --detailed-exitcodes -e \"cron { 'test' : ensure => present, command => '/bin/true', minute => 3 }\"") do
    its(:exit_status) { should eq 2 }
    its(:stdout) { should match %r{/Stage\[main\]/Main/Cron\[test\]/ensure: created} }

    describe cron do
      it { should have_entry '3 * * * * /bin/true' }
    end
  end

  describe command("puppet apply --detailed-exitcodes -e \"cron { 'test' : ensure => absent }\"") do
    its(:exit_status) { should eq 2 }
    its(:stdout) { should match %r{/Stage\[main\]/Main/Cron\[test\]/ensure: removed} }

    describe cron do
      it { should_not have_entry '3 * * * * /bin/true' }
    end
  end
end

describe "test augeas core module" do
  tmpdir = Dir.mktmpdir('puppet')

  manifest = <<~EOT
  augeas { 'augeas_spec':
    incl    => '#{tmpdir}/puppet.conf',
    lens    => 'Puppet.lns',
    changes => 'set /files/#{tmpdir}/puppet.conf/main/bar qux',
  }
  EOT
  File.open("#{tmpdir}/augeas.pp", 'w') { |f| f.write(manifest) }

  conf = <<~EOT
  [main]
  bar = baz
  EOT
  File.open("#{tmpdir}/puppet.conf", 'w') { |f| f.write(conf) }

  describe "changing config file parameter" do
    describe command("puppet apply --debug --detailed-exitcodes #{tmpdir}/augeas.pp") do
      its(:exit_status) { should eq 2 }
      its(:stdout) { should match %r{/Stage\[main\]/Main/Augeas\[augeas_spec\]/returns: executed successfully} }

      describe file("#{tmpdir}/puppet.conf") do
        its(:content) { should eq "[main]\nbar = qux\n" }
      end
    end
  end
end
